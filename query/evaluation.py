from typing import List

def is_jaccard_true(search_video_motion_tags, search_pool_video_motion_tags, threshold=0.5) -> bool:
  """This function defines a Jaccard distance inspired notion of a true positive based on the motion tags.
  
  Parameters
  ----------
  search_video_motion_tags : set
    The set of motion tags associated with the search video.
  search_pool_motion_tags : set
    The set of motion tags associated with the retrieved video.
  threshold : float
    The threshold over which the retrieved video is considered irrelevant, yielding loss 1.

  Returns
  -------
  loss : bool
    True if retrieved video is relevant (true positive), False if it is irrelevant (false positive).
  """
  return len(search_video_motion_tags.intersection(search_pool_video_motion_tags)) / len(search_video_motion_tags) >= threshold

def jaccard_inspired_loss(search_video_motion_tags, search_pool_video_motion_tags, loss_threshold=0.5) -> int:
  return int(is_jaccard_true(search_video_motion_tags, search_pool_video_motion_tags, threshold=loss_threshold))

# TODO is shoehorning this into a strict ML approach relevant??? Should take a page from Google's book, probably
def compute_confusion_matrix(search_video_filename: str, retrieved_videos_filenames: List[str], search_pool_annotations, true_func=is_jaccard_true):
  """For a given search query input video and the retrieved filenames, compute the confusion matrix.
  
  Parameters
  ----------
  search_video_filename : str
    The filename of the search term video.
  retrieved_videos_filenames: list of str
    The filenames of the retrieved videos.
  search_pool_annotations : pandas dataframe
    The dataframe containing the annotations for all the videos in the search pool.
  true_func : set of str, set of str -> bool
    A function defining a notion of true or false based on motion tags.

  Returns
  -------
  confusion_matrix : dict of int
    A dictionary storing the number of TP, FP, TN, FN, where all retrieved videos from the pool are positive and the rest negative.
  """
  # Function to define whether true or false based on motion tags
  # TODO as soon as the dinosaurs over at numpy support Python 3.9 put the removesuffix back in
  is_positive = search_pool_annotations.apply(lambda row: row.name[:-4] in retrieved_videos_filenames, axis=1)
  search_video_motion_tags = search_pool_annotations['motion tags'].loc['{}.mov'.format(search_video_filename)]
  jaccard_true = search_pool_annotations['motion tags'].apply(lambda search_pool_video_motion_tags: is_jaccard_true(search_video_motion_tags, search_pool_video_motion_tags))
  
  is_true = jaccard_true.where(is_positive, other=~jaccard_true)

  n_true_positive = sum(is_true & is_positive)
  n_true_negative = sum(is_true & ~is_positive)
  n_false_positive = sum(~is_true & is_positive)
  n_false_negative = sum(~is_true & ~is_positive)

  return {'tp': n_true_positive, 'tn': n_true_negative, 'fp': n_false_positive, 'fn': n_false_negative}

def compute_loss_and_metrics(search_pool_similarities, search_pool_annotations, search_func, loss_func=jaccard_inspired_loss, metrics={}, **search_params):
  """Compute the total loss of the retrieval method for all the videos in the search pool.
  
  Parameters
  ----------
  search_pool_similarities : dict of dicts
    A dictionary of dictionaries containing the similarity between all pairs of videos in the search pool.
  search_pool_annotations : pandas dataframe
    The dataframe containing the annotations for all the videos in the search pool.
  search_func : function
    A function which returns a set of result videos.
  search_params : iterable
    The parameters to the search function other than the search video name and search pool similarities.
  loss_func : set of str, set of str, float -> float
    The loss function.
  metrics : dict of functions
    A dictionary of functions on a confusion matrix representing performance metrics of the retrieval engine.

  Returns
  -------
  total_loss : float
    The total loss (TODO what kind of loss?) of the retrieval method for the search pool.
  total_metric_values : dict of float
    The total values of the metrics for the search pool.
  avg_loss : float
    The average loss of the retrieval method for the search pool.
  avg_metric_values : dict of float
    The average values of the metrics for the search pool.
  """
  total_loss = 0.0
  total_metric_values = {}
  for metric_name in metrics.keys():
      total_metric_values[metric_name] = 0.0
  
  # Get the top k results for the entire search pool
  for video_name in search_pool_similarities.keys():
    result_filenames = search_func(video_name, search_pool_similarities, **search_params)
    search_video_motion_tags = search_pool_annotations['motion tags'].loc['{}.mov'.format(video_name)]
    result_videos_motion_tags = search_pool_annotations['motion tags'].loc[['{}.mov'.format(result_filename) for result_filename in result_filenames]]

    total_video_loss = result_videos_motion_tags.apply(lambda result_video_motion_tags: loss_func(set(result_video_motion_tags), set(search_video_motion_tags))).agg(sum)
    total_loss += total_video_loss

    confusion_matrix = compute_confusion_matrix(video_name, result_filenames, search_pool_annotations)
    for metric_name, metric_func in metrics.items():
      total_metric_values[metric_name] += metric_func(confusion_matrix)

  avg_loss = total_loss / len(search_pool_similarities)
  avg_metric_values = {metric_name: total_metric_value / len(search_pool_similarities) for metric_name, total_metric_value in total_metric_values.items()}

  return total_loss, total_metric_values, avg_loss, avg_metric_values

def accuracy(confusion_matrix):
  return (confusion_matrix['tp'] + confusion_matrix['tn']) / (confusion_matrix['tp'] + confusion_matrix['tn'] + confusion_matrix['fp'] + confusion_matrix['fn'])

def recall(confusion_matrix):
  return confusion_matrix['tp'] / (confusion_matrix['tp'] + confusion_matrix['fn']) if confusion_matrix['tp'] + confusion_matrix['fn'] > 0 else 0

def precision(confusion_matrix):
  return confusion_matrix['tp'] / (confusion_matrix['tp'] + confusion_matrix['fp']) if confusion_matrix['tp'] + confusion_matrix['fp'] > 0 else 0

def f1_score(confusion_matrix):
  recall_score = recall(confusion_matrix)
  precision_score = precision(confusion_matrix)
  return 2 / (1 / recall_score + 1 / precision_score) if recall_score > 0 and precision_score > 0 else 0