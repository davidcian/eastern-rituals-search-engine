import glob
import numpy as np
import os
import pickle

from argparse import ArgumentParser
from pandas import read_csv
from tqdm import tqdm

# TODO multi-term search??? could be quite cool!
# TODO add human validation of search engine, also cool

def search_for(search_video_file_name: str, search_pool_similarities, top_k=5):
  """Find the top k most similar videos to the search term video using precomputed similarities.

  Parameters
  ----------
  search_video_file_name : str
    The file name of the search term video.
  search_pool_similarities : dict of dicts
    A dictionary of dictionaries containing the similarity between all pairs of videos in the search pool.

  Returns
  -------
  top_k_filenames : list of str
    The k videos from the search pool most similar to the search term video.
  """
  search_pool_similarities_to_video = search_pool_similarities[search_video_file_name]
  return sorted(search_pool_similarities_to_video.keys(), key=lambda search_pool_video_name: search_pool_similarities_to_video[search_pool_video_name])[:top_k]

def search_for_natural(search_video_file_name: str, search_pool_similarities, threshold=3.0):
  """Find the videos more similar than a threshold to the search term video using precomputed similarities.

  Parameters
  ----------
  search_video_file_name : str
    The file name of the search term video.
  search_pool_similarities : dict of dicts
    A dictionary of dictionaries containing the similarity between all pairs of videos in the search pool.
  threshold : float
    The threshold distance over which results are not considered relevant anymore.

  Returns
  -------
  result_filenames : list of str
    The videos from the search pool most similar to the search term video.
  """
  search_pool_similarities_to_video = search_pool_similarities[search_video_file_name]
  results = {video_name: similarity for video_name, similarity in search_pool_similarities_to_video.items() if similarity < threshold}
  return sorted(results.keys(), key=lambda video_name: results[video_name])

# Main search algorithm
if __name__ == '__main__':
  parser = ArgumentParser(description="Search a collection of videos for a video search term")

  # Positional command line arguments
  parser.add_argument('input_name', type=str, help="the name of the input video to search for")
  parser.add_argument('pool_search_path', type=str, help="the path of the search pool folder containing folders for pose data")
  parser.add_argument('pool_similarities_path', type=str, help="the path of the similarities file for the chosen search pool")

  # Optional command line arguments
  parser.add_argument('--top-k', type=int, default=5, help="the number of top results to return")

  # Parse the command line arguments
  cli_args = parser.parse_args()

  # Read in the annotations
  annotations_path_name = glob.glob('{}/annotation_*.csv'.format(cli_args.pool_search_path))[0]
  # TODO remove space from filename column label in data annotations CSV
  annotations = read_csv(annotations_path_name, index_col='filename ')
  annotations['motion tags'] = annotations['motion tags'].apply(lambda motion_tags_str: set(motion_tags_str.split(',')))

  # Compute the similarity between the search video and all videos in the search pool
  #print("Computing similarities...")
  #search_pool_similarities = {}
  #for video_name in tqdm(search_pool_video_features.keys()):
    #print("Computing similarity to video {}".format(video_name))
    #search_pool_similarities[video_name] = compute_similarity(search_pool_video_features[cli_args.input_name], search_pool_video_features[video_name])
    #print("Similarity with {}: {}".format(video_name, search_pool_similarities[video_name]))
  #print("Computing similarities done!")

  # TODO modularize similarity computing & loading
  # Load similarity between all the videos in the search pool
  with open(cli_args.pool_similarities_path, 'rb') as similarities_file:
    search_pool_similarities = pickle.load(similarities_file)

  # Assign the query results
  result_filenames = search_for_natural(cli_args.input_name, search_pool_similarities)

  # Pretty-print the query results
  print("Search results for video {}, with motion tags {}:".format(cli_args.input_name, annotations['motion tags'].loc['{}.mov'.format(cli_args.input_name)]))
  for i, file_name in enumerate(result_filenames):
    print("{}. {}; similarity: {}; motion tags: {}".format(i, file_name, search_pool_similarities[cli_args.input_name][file_name], annotations['motion tags'].loc['{}.mov'.format(file_name)]))
