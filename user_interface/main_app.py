import glob
import matplotlib.pyplot as plt
import numpy as np
import pickle
import scipy.spatial.distance as ssd

from flask import Flask, render_template, request, send_file
from pandas import read_csv

from database.clustering import number_of_clusters
from database.pool_analytics import search_pool_size, search_pool_mem_size
from query.evaluation import compute_loss_and_metrics, accuracy, recall, precision, f1_score
from query.search import search_for, search_for_natural

app = Flask(__name__)

pool_source_paths = []

@app.route('/')
def render_homepage():
  return render_template('index.html')

@app.route('/', methods=['POST'])
def render_serp():
  search_video_name = request.form['video']

  with open("similarities_side.pkl", 'rb') as similarities_file:
    search_pool_similarities = pickle.load(similarities_file)
  
  result_video_names = search_for(search_video_name, search_pool_similarities)

  return render_template('serp.html', search_video_name=search_video_name, result_video_names=result_video_names)

@app.route('/database')
def render_database():
  global pool_source_paths
  pool_size = sum(map(search_pool_size, pool_source_paths))
  pool_mem = sum(map(search_pool_mem_size, pool_source_paths))

  return render_template('database.html', total_search_pool_videos=pool_size, total_search_pool_mem=pool_mem)

@app.route('/configuration')
def render_configuration():
  return render_template('configuration.html')

@app.route('/configuration', methods=['POST'])
def update_configuration():
  global pool_source_paths
  pool_source_paths.append(request.form['source_folder_path'])

  return render_configuration()

@app.route('/run_benchmark')
def run_benchmark():
  # Read in the annotations
  annotations_path_name = glob.glob('{}/annotation_*.csv'.format(pool_source_paths[0]))[0]
  # TODO remove space from filename column label in data annotations CSV
  annotations = read_csv(annotations_path_name, index_col='filename ')
  annotations['motion tags'] = annotations['motion tags'].apply(lambda motion_tags_str: set(motion_tags_str.split(',')))

  # Load similarity between all the videos in the search pool
  with open("similarities_side.pkl", 'rb') as similarities_file:
    search_pool_similarities = pickle.load(similarities_file)

  # Compute the loss and metrics over the entire search pool
  search_pool_total_loss, search_pool_total_metrics, search_pool_avg_loss, search_pool_avg_metrics = compute_loss_and_metrics(
    search_pool_similarities, annotations, search_for_natural, metrics={'accuracy': accuracy, 'recall': recall, 'precision': precision, 'f1 score': f1_score}, threshold=5.0)
  
  return {"search_pool_total_loss": search_pool_total_loss, "search_pool_total_metrics": search_pool_total_metrics, "search_pool_avg_loss": search_pool_avg_loss, "search_pool_avg_metrics": search_pool_avg_metrics}

@app.route('/run_cluster')
def run_cluster():
  with open("similarities_side.pkl", 'rb') as similarities_file:
    search_pool_similarities = pickle.load(similarities_file)

  # Transform dict of dicts into redundant distance matrix
  distance_matrix = np.zeros((len(search_pool_similarities), len(search_pool_similarities)))

  sorted_search_pool_similarities = sorted(search_pool_similarities.items())

  for i, (video_name, similarities_dict) in enumerate(sorted_search_pool_similarities):
    sorted_similarities_items = sorted(similarities_dict.items())
    sorted_similarities = [similarity for video_name, similarity in sorted_similarities_items]
    
    distance_matrix[i] = sorted_similarities

  rounded_distance_matrix = np.around(distance_matrix, decimals=1)

  # Set diagonal to 0
  np.fill_diagonal(rounded_distance_matrix, 0.0)

  number_of_clusters(ssd.squareform(rounded_distance_matrix), [k for k, v in sorted_search_pool_similarities])
  plt.savefig(r"D:\dendrogram.png")

  return {"useless": "useless"}

@app.route('/video_upload/<path:filename>')
def video_upload(filename):
  return send_file("D:/eastern-rituals-data/em_data_cs433/posecuts_sideview/" + filename + ".mov")

@app.route('/image_upload/<path:filename>')
def image_upload(filename):
  return send_file("D:/" + filename + ".png")

if __name__ == '__main__':
  app.run(debug=True)