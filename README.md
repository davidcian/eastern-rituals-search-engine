# ERSE: Eastern Rituals Search Engine

ERSE is a content-based information retrieval (CBIR) system designed to facilitate the task of exploring a dataset of video recordings of humans.

## Current context

ERSE is currently being used within the [EncodingActs](https://www.epfl.ch/labs/emplus/encodingacts/) project at EPFL, which concerns itself with recognizing motions in ancient Eastern rituals. The EncodingActs project has built up a massive archive of reenactments of several Eastern rituals, played by professional actors and based on detailed 2nd CE documents describing them. As an exploratory venture, ERSE has been used with a fraction of the EncodingActs data. The main goal of ERSE is to act as a kind of search engine for video recordings of human ritualistic behavior.

## Quickstart

Note: the raw EncodingActs data is confidential. As such, we only provide processed data, limiting the functionality of ERSE out of the box. However, we encourage you to try out ERSE with video data of your own in order to experience its full capabilities, which is the process described in the quickstart below.

---

### Prerequisites

Conda

A folder `data_folder` with videos.

---

### Data setup

Clone the OpenPose repository.

`clone git@github.com:CMU-Perceptual-Computing-Lab/openpose.git`

Create a folder to store pose data in.

`mkdir pose_data`

Clone the ERSE repository.

`git clone git@gitlab.com:davidcian/eastern-rituals-search-engine.git`

Move into the cloned repository.

`cd eastern-rituals-search-engine`

Create an adequate conda environment.

`conda env create --file environment.yaml`

Activate the `erse` conda environment.

`conda activate erse`

Run the `extract_pose.py` script to extract pose data from the videos (the argument names are pretty self-explanatory, the OpenPose folder is the root of the previously cloned repository).

`python extract_pose.py data_folder pose_data_folder openpose_folder`

Run the `extract_features.py` script to extract features from the pose data.

`extract_features.py pose_data_folder features_folder`

Run the `compute_similarity.py` script to compute the similarities between the videos.

`compute_similarity.py features_folder similarities_file`

### First steps

Run the web app.

`python -m user_interface.main_app`

Navigate to [this URL](http://127.0.0.1:5000/).

Navigate to the Configuration tab.

Enter the path to the video data folder in the field and press the Submit button.

Navigate to the Search tab. You can enter the name of one of the videos in the search bar.

The results page will display the most similar videos.