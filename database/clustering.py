import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from sklearn.cluster import AgglomerativeClustering
import scipy.cluster.hierarchy as shc

def number_of_clusters(distance_matrix, labels=[]):
  """
  We are going to use a dendrogram to visualize the groupings and deduce
  the optimal number of clusters by:
  1-Determine the largest vertical distance that doesn’t intersect any of the other clusters
  2-draw an horizantal line
  3-the number of clusters is the number of intersections with the vertical lines
  """
  plt.figure(figsize=(10, 7))
  plt.title("Dendrogram")
  dendrogram = shc.dendrogram(shc.linkage(distance_matrix, method='ward'), labels=labels, orientation='right')
  return dendrogram

def clustering(distance_matrix, n):
  """
  we use the AgglomerativeClustering class from the "sklearn.cluster" library.
  The number of parameters is set to n parameter(that we found in the previous function) while the affinity
  (distance between datapoints) is set to "euclidean"
  """
  cluster = AgglomerativeClustering(n_clusters=n, affinity='euclidean', linkage='ward')
  # returns the names of the clusters that each data point belongs to
  return cluster.fit_predict(distance_matrix)
