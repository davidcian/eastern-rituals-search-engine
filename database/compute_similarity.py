"""Compute the similarity between all pairs of videos in the search pool."""

import numpy as np
import os
import pickle

from argparse import ArgumentParser
from tqdm import tqdm

def compute_similarity_simple_cartesian(video1_features, video2_features) -> float:
  """Compute the similarity between two videos based on a simple Cartesian distance.

  Parameters
  ----------
  video1_features : numpy array
    The first video features as a 2D array, where a row represents the features for a frame.
  video2 : numpy array
    The second video features as a 2D array, where a row represents the features for a frame.

  Returns
  -------
  similarity : float
    The similarity between the two videos.
  """
  similarity_sum = 0.0
  for i in range(video1_features.shape[0]):
    for j in range(video2_features.shape[0]):
      similarity_sum += np.linalg.norm(video1_features[i] - video2_features[j])

  similarity_mean = similarity_sum / (video1_features.shape[0] * video2_features.shape[0])

  return similarity_mean

def compute_search_pool_similarities(search_pool_video_features, similarity_func=compute_similarity_simple_cartesian):
  """Compute similarities between all the videos in the search pool using precomputed features.

  Parameters
  ----------
  search_pool_video_features : dict
    A dictionary containing the features of every video in the search pool.
  similarity_func : numpy array, numpy array -> float
    A function computing the similarity between two videos based on their features.

  Returns
  -------
  search_pool_similarities : dict of dicts
    A dictionary of dictionaries containing the similarity between all pairs of videos in the search pool.
  """
  search_pool_similarities = {}

  # TODO make more efficient using numpy or pandas or something
  for video_name_1, video_features_1 in tqdm(search_pool_video_features.items()):
    similarities_to_video = {}
    for video_name_2, video_features_2 in search_pool_video_features.items():
      similarities_to_video[video_name_2] = similarity_func(video_features_1, video_features_2)
    
    search_pool_similarities[video_name_1] = similarities_to_video

  return search_pool_similarities

if __name__ == '__main__':
  parser = ArgumentParser(description="Compute the similarity between all pairs of videos in the search pool")

  # Positional command line arguments
  parser.add_argument('pool_features_folder_path', type=str, help="the path of the folder containing the extracted features of the search pool")
  parser.add_argument('similarity_output_file_path', type=str, help="the path of the file to save the similarities to")

  # Parse the command line arguments
  cli_args = parser.parse_args()

  # Read in the pose data
  print("Loading all the pool features...")
  search_pool_video_features = {}
  for video_features in tqdm(os.scandir(cli_args.pool_features_folder_path)):
    search_pool_video_features[video_features.name.split('.')[0]] = np.load(video_features)
  print("Loading all the pool features done!")

  # Compute the similarity between all the videos in the search pool
  print("Computing similarities...")
  search_pool_similarities = compute_search_pool_similarities(search_pool_video_features)
  print("Computing similarities done!")
  # Save the computed similarities
  with open(cli_args.similarity_output_file_path, 'wb') as similarities_file:
    pickle.dump(search_pool_similarities, similarities_file)
  print("Saved similarities to {}.".format(cli_args.similarity_output_file_path))