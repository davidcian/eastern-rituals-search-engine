"""Run all kinds of pool-wide analytics on a search pool."""

import glob
import numpy as np
import os
import pickle

from argparse import ArgumentParser

from database.clustering import number_of_clusters

def cluster_search_pool(search_pool_video_features):
  """Cluster all the search pool videos into separate groups.
  
  Parameters
  ----------
  search_pool_video_features : dict
    A dictionary containing the features of every video in the search pool.

  Returns
  -------
  cluster_model : sklearn.cluster.KMeans
    A model allowing to predict which cluster a video belongs to.
  """

  return 0

def search_pool_size(search_pool_folder_path: str) -> int:
  """Find the number of videos in the search pool.
  
  Parameters
  ----------
  search_pool_folder_path : str
    The path to the search pool folder.

  Returns
  -------
  search_pool_size : int
    The number of videos in the search pool.
  """

  video_paths = glob.glob('{}/*.mov'.format(search_pool_folder_path))
  return len(video_paths)

def search_pool_mem_size(search_pool_folder_path: str) -> int:
  """Find the size of the search pool in bytes.
  
  Parameters
  ----------
  search_pool_folder_path : str
    The path to the search pool folder.

  Returns
  -------
  search_pool_mem_size : int
    The size of the search pool in bytes.
  """

  video_paths = glob.glob('{}/*.mov'.format(search_pool_folder_path))
  mem_size = sum([os.stat(video_path).st_size for video_path in video_paths])
  return mem_size