"""Extract features in NumPy format from pose data to a search pool features folder."""

import math
import numpy as np
import os

from argparse import ArgumentParser
from pandas import read_json
from tensorflow.keras import Input, Model, Sequential
from tensorflow.keras.layers import LSTM, Masking, TimeDistributed, Dense, RepeatVector
from tensorflow.keras.models import load_model
from tensorflow.keras.preprocessing.sequence import pad_sequences

# joints_id[joint_name] = (point_ref_1, point_ref_2)

# could only define 14 body segments instead of 17 from Query-by-Dancing,
# although due to clothing probably most angles except spine and
# shoulders will be quite inaccurate

body_segments = {
  "neck": (0, 1),
  "shoulder_left": (5, 6),
  "shoulder_right": (2, 3),
  "elbow_left": (6, 7),
  "elbow_right": (3, 4),
  "spine": (1, 8),
  "hips_left": (12, 13),
  "hips_right": (9, 10),
  "knee_left": (13, 14),
  "knee_right": (10, 11),
  "ankle_left": (14, 19),
  "ankle_right": (11, 22),
  "toes_left": (19, 20),
  "toes_right": (22, 23)
}

relevant_body_segments = {
  "neck": (0, 1),
  "shoulder_left": (5, 6),
  "shoulder_right": (2, 3),
  "elbow_left": (6, 7),
  "elbow_right": (3, 4),
  "spine": (1, 8),
  "hips_left": (12, 13),
  "hips_right": (9, 10),
  "knee_left": (13, 14),
  "knee_right": (10, 11)
}

minimal_body_segments = {
  "neck": (0, 1),
  "shoulder_left": (5, 6),
  "elbow_left": (6, 7),
  "spine": (1, 8),
  "hips_left": (12, 13),
  "knee_left": (13, 14)
}

def joint_angle_from_coordinates(joint_1_X, joint_1_Y, joint_2_X, joint_2_Y):
  """Compute angle between two joints given their coordinates in the "screen" coordinate system.

  Parameters
  ----------
  joint_1_coordinates : tuple of float
    The X and Y coordinates of joint 1
  joint_2_coordinates : tuple of float
    The X and Y coordinates of joint 2

  Returns
  -------
  joint_angle : float
    The clockwise angle of the limb from the bottom-top line starting at joint 1, in radians
  """
  # If either joint is not found set the angle to 0
  if (joint_1_X == 0 and joint_1_Y == 0) or (joint_2_X == 0 and joint_2_Y == 0):
    joint_angle = 0
  else:
    x_diff = joint_2_X - joint_1_X
    y_diff = joint_1_Y - joint_2_Y
    joint_angle = math.atan2(y_diff, x_diff)

  return joint_angle

def angle_to_cartesian_coordinates(joint_angle):
  """Decompose a limb angle into its Cartesian components in the "screen" coordinate system.

  Parameters
  ----------
  joint_angle : float
    A limb angle

  Returns
  -------
  x : float
    The X component of the angle
  y : float
    The Y component of the angle
  """
  return math.sin(joint_angle), -math.cos(joint_angle)

def extract_joint_angles(pose_data, body_segments):
  """Extract the joint angles in a video from the pose data.

  Parameters
  ----------
  pose_data : numpy array
    A 2D array, where a row represents the pose data (2D joint coordinates) for a frame.
  body_segments : list of pairs of int
    A list of pairs of keypoint indices defining limbs.

  Returns
  -------
  joint_angles : numpy array
    A 2D array, where a row represents the joint angles (in radians) for a frame.
  """
  joint_angles = []
  for segment_name in body_segments.keys():
    joint_1_idx, joint_2_idx = body_segments[segment_name]
    joint_1_coordinates = pose_data[:, joint_1_idx * 2], pose_data[:, joint_1_idx * 2 + 1]
    joint_2_coordinates = pose_data[:, joint_2_idx * 2], pose_data[:, joint_2_idx * 2 + 1]

    joint_angle_from_coordinates_vectorized = np.vectorize(joint_angle_from_coordinates)
    joint_angle = joint_angle_from_coordinates_vectorized(joint_1_coordinates[0], joint_1_coordinates[1], joint_2_coordinates[0], joint_2_coordinates[1])
    joint_angles.append(joint_angle)

  joint_angles_array = np.vstack(joint_angles).T

  return joint_angles_array

def decompose_joint_angles(joint_angles):
  """Decompose the joint angles into Cartesian coordinates.

  Parameters
  ----------
  joint_angles : numpy array
    A 2D array, where a row represents the joint angles (in radians) for a frame.

  Returns
  -------
  decomposed_joint_angles : numpy array
    A 2D array, where a row represents the decomposed joint angles for a frame.
  """
  angle_to_cartesian_coordinates_vectorized = np.vectorize(angle_to_cartesian_coordinates)
  decomposed_joint_angles = angle_to_cartesian_coordinates_vectorized(joint_angles)

  # Interleave the two resulting X and Y ndarrays
  decomposed_joint_angles = np.dstack(decomposed_joint_angles).reshape(decomposed_joint_angles[0].shape[0], -1)

  return decomposed_joint_angles

def extract_angular_joint_speeds(decomposed_joint_angles):
  """Extract the angular joint speeds from decomposed joint angles.

  Parameters
  ----------
  decomposed_joint_angles : numpy array
    A 2D array, where a row represents the decomposed joint angles for a frame.

  Returns
  -------
  angular_joint_speeds : numpy array
    A 2D array, where a row represents the angular joint speeds for a frame.
  """
  previous_decomposed_joint_angles = np.roll(decomposed_joint_angles, 1, axis=0)
  # TODO is setting angles to 0 for moment before start of video a good way of doing this?
  # TODO use np.diff for discrete diff instead?
  previous_decomposed_joint_angles[0] = 0

  angular_joint_speeds = np.abs(decomposed_joint_angles - previous_decomposed_joint_angles)

  return angular_joint_speeds

def extract_angular_joint_accelerations(angular_joint_speeds):
  """Extract the angular joint accelerations from decomposed joint angles.

  Parameters
  ----------
  angular_joint_speeds : numpy array
    A 2D array, where a row represents the angular joint speeds for a frame.

  Returns
  -------
  angular_joint_accelerations : numpy array
    A 2D array, where a row represents the angular joint accelerations for a frame.
  """
  previous_angular_joint_speeds = np.roll(angular_joint_speeds, 1, axis=0)
  # TODO same question as above
  # TODO refactor as one method for differentiation or something
  previous_angular_joint_speeds[0] = 0

  angular_joint_accelerations = np.abs(angular_joint_speeds - previous_angular_joint_speeds)

  return angular_joint_accelerations

def extract_features_from_pose(pose_data, body_segments):
  """Extract a feature vector for all the frames of the pose data of a video.

  Parameters
  ----------
  pose_data : numpy array
    A 2D array, where a row represents the pose data (2D joint coordinates) for a frame.
  body_segments : list of pairs of int
    A list of pairs of keypoint indices defining limbs.

  Returns
  -------
  features : numpy array
    A 2D array, where a row represents the features for a frame.
  """
  # Extract joint angles as 2d numpy array
  joint_angles = extract_joint_angles(pose_data, body_segments)
  # Decompose joint angles as 2d numpy array
  joint_angles = decompose_joint_angles(joint_angles)
  # Extract angular joint speeds as 2d numpy array
  joint_angular_speeds = extract_angular_joint_speeds(joint_angles)
  # Extract angular joint acceleration as 2d numpy array
  joint_angular_accelerations = extract_angular_joint_accelerations(joint_angles)

  video_features = np.hstack((joint_angles, joint_angular_speeds, joint_angular_accelerations))

  return video_features

def load_pose_data(video_path):
  """Load the pose data of a video into a NumPy array.

  Parameters
  ----------
  video_path : str
    The path to a folder containing one JSON file per frame.

  Returns
  -------
  pose_data : numpy array
    A 2D array, where each row represents the joint coordinates for a frame.
  """
  video_pose_data = {}

  for frame_file in os.scandir(video_path):
    frame_pose_data = read_json(frame_file.path)
    video_pose_data[frame_file.name] = frame_pose_data

  chronological_video_pose_data = [v['people'] for k, v in sorted(video_pose_data.items())]

  frame_pose_arrays = []

  for frame_pose_data in chronological_video_pose_data:
    if len(frame_pose_data) == 0:
      # If no person was identified, set all the 2 * 25 joint coordinates to 0
      person_pose_data_no_confidence = np.zeros(2 * 25)
    else:
      # TODO take into account multiple people!!!!!!!! SUPER IMPORTANT!!!!!!
      # TODO take into account confidence???
      person_pose_data = frame_pose_data[0]['pose_keypoints_2d']
      person_pose_data = np.array(person_pose_data)

      person_pose_data_confidence_mask = np.ones(len(person_pose_data), dtype=bool)
      person_pose_data_confidence_mask[2::3] = False

      person_pose_data_no_confidence = person_pose_data[person_pose_data_confidence_mask]

    frame_pose_arrays.append(person_pose_data_no_confidence)

  video_pose_data_array = np.vstack(frame_pose_arrays)

  return video_pose_data_array

def load_all_pose_data(pool_pose_folder_path):
  """Load the pose data of every video in the search pool into a NumPy array.
  
  Parameters
  ----------
  pool_pose_folder_path : str
    The path of the folder containing the pose data.

  Returns
  -------
  pose_data : dict of numpy array
    The pose data for every video in the search pool.
  """
  pose_data = {}
  for video_file in os.scandir(pool_pose_folder_path):
    pose_data[video_file.name] = load_pose_data(video_file.path)

  return pose_data

def train_LSTM_autoencoder(pose_data_dict, previous_model=None, batch_size=8, epochs=10):
  """Train an LSTM autoencoder to reconstruct pose data.

  Parameters
  ----------
  pose_data_dict : dict of numpy array
    The pose data for every video in the search pool.
  previous_model : tf.keras.Model
    A pre-trained LSTM encoder decoder to train some more.
  batch_size : int
    The size of the minibatches.
  epochs : int
    The number of epochs to train for

  Returns
  -------
  encoder_model : tf.keras.Model
    A model allowing encoding of raw pose data to a fixed length representation.
  model : tf.keras.Model
    The entire trained LSTM encoder decoder for future training.
  """
  pose_data = [video_pose_data for video_name, video_pose_data in pose_data_dict.items()]
  max_seq_len = max([video_pose_data.shape[0] for video_pose_data in pose_data])
  pose_data_padded = [np.pad(video_pose_data, ((0, max_seq_len - video_pose_data.shape[0]), (0, 0))) for video_pose_data in pose_data]
  pose_data_stacked = np.stack(pose_data_padded)
  n_features = pose_data[0].shape[1]
  
  inputs = Input(shape=(max_seq_len, n_features))
  encoder = Masking()(inputs)
  encoder = LSTM(2048, input_shape=(None, n_features))(encoder)
  encoder = RepeatVector(max_seq_len)(encoder)
  decoder = LSTM(n_features, return_sequences=True)(encoder)
  decoder = TimeDistributed(Dense(1))(decoder)
  model = Model(inputs=inputs, outputs=decoder)
  model.compile(optimizer='adam', loss='mse')

  model.fit(x=pose_data_stacked, y=pose_data_stacked, batch_size=batch_size, epochs=epochs)

  encoder_model = Model(inputs=inputs, outputs=encoder)

  return encoder_model, model

if __name__ == '__main__':
  parser = ArgumentParser(description="Extract features from pose data")

  # Positional command line arguments
  parser.add_argument('pool_pose_folder_path', type=str, help="the path to the folder containing the raw pose data for the search pool")
  parser.add_argument('output_folder_path', type=str, help="the path to the output folder")

  # Parse the command line arguments
  cli_args = parser.parse_args()

  # Load the raw pose data
  pose_data = load_all_pose_data(cli_args.pool_pose_folder_path)

  # Autoencode the raw pose data
  #print("Encoding raw pose data...")
  #encoder, encoder_decoder = train_LSTM_autoencoder(pose_data, previous_model=load_model("encoder_decoder_model"), epochs=5)
  #encoder.save("encoder_model")
  #encoder_decoder.save("encoder_decoder_model")
  #print("Done encoding!")

  # Extract features from the raw pose data
  search_pool_video_features = {}
  for video_name in pose_data.keys():
    search_pool_video_features[video_name] = extract_features_from_pose(pose_data[video_name], body_segments)

  # Save the extracted features
  for video_name in pose_data.keys():
    np.save(os.path.join(cli_args.output_folder_path, video_name), search_pool_video_features[video_name])