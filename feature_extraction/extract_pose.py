import os
import subprocess

from argparse import ArgumentParser

parser = ArgumentParser(description="Extract pose data from each video in a folder")

# Positional command line arguments
parser.add_argument('folder_path', type=str, help="the path of the folder for which to extract pose data")
parser.add_argument('output_path', type=str, help="the path of the output folder")
parser.add_argument('openpose_path', type=str, help="the path of the openpose folder")

# Parse the command line arguments
cli_args = parser.parse_args()

# TODO make using openpose easier

openpose_path = os.path.join(cli_args.openpose_path, "openpose")
os.chdir(openpose_path)
for video in os.scandir(cli_args.folder_path):
  video_basename, video_extension = video.name[:-4], video.name[-4:]
  if video_extension in ['.mov']:
    subprocess.run([os.path.join(openpose_path, "bin", "OpenPoseDemo.exe"), "--video", video.path, "--write_json", os.path.join(cli_args.output_path, video_basename)], check=True)