﻿$videos = Get-ChildItem D:\eastern-rituals-data\em_data_cs433\posecuts_sideview *.mov

foreach ($video in $videos) {
    New-Item -Path D:\eastern-rituals-pose-data -Name $video.BaseName -ItemType "directory"
}

Push-Location -Path D:\open-pose-portable\openpose
foreach ($video in $videos) {
    D:\open-pose-portable\openpose\bin\OpenPoseDemo.exe --video $video.FullName --write_json D:\eastern-rituals-pose-data\$($video.BaseName)
}
Pop-Location